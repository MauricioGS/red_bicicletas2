

var map = L.map('main_map').setView([4.0949,-76.170], 13);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
maxZoom: 18
}).addTo(map);

L.control.scale().addTo(map);

L.marker([4.0949,-76.170],{draggable: true}).addTo(map);
L.marker([4.0452,-76.110],{draggable: true}).addTo(map);
L.marker([4.0854,-76.197],{draggable: true}).addTo(map);