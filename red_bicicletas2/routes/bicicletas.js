var express = require('express');
var router = express.Router();
var biciletaController = require ('../controllers/bicicleta');

router.get('/', biciletaController.bicicleta_list);
router.get('/create', biciletaController.bicicleta_create_get);
router.post('/create', biciletaController.bicicleta_create_post);
router.get('/:id/update', biciletaController.bicicleta_update_get);
router.post('/:id/update', biciletaController.bicicleta_update_post);
router.post('/:id/delete', biciletaController.bicicleta_delete_post);


console.log('Entra arreglo en routes bicicleta.js');

module.exports = router;