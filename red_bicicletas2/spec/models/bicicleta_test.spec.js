var Bicicleta = require('../../models/bicicleta');
const { findById } = require('../../models/bicicleta');

beforeEach(() => {Bicicleta.allBicis = [];});

describe("Bicicleta.allBicis", () => {
    it("comienza vacio", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe("Bicicleta.add", () => {
    it("Agregamos una", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, "rojo", "urbana", [4.083983, -76.186578]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe("Bicicleta.findById", () => {
    it("Debe de volver la bici con id=1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici  = new Bicicleta(1, "verde", "urbana");
        var aBici2 = new Bicicleta(2, "azul", "montaña");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    })
})

describe("Bicicleta.removeById", () => {
    it("Debe eliminar la bici con id=1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici  = new Bicicleta(1, "verde", "urbana");
        Bicicleta.add(aBici);

        expect(Bicicleta.allBicis.length).toBe(1);
        
        var targetBici = findById(1);
        expect(targetBici.id).toBe(1);
        
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
        

    });
});