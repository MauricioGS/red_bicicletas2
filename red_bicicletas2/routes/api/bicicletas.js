var express = require('express');
var router = express.Router();
var biciletaController = require ('../../controllers/api/bicicletaControllerAPI');

const { route } = require('..');

router.get('/',biciletaController.bicicleta_list);
router.post('/create',biciletaController.bicicleta_create);
router.delete('/delete',biciletaController.bicicleta_delete);

module.exports = router;