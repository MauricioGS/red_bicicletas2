var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe("Bicicleta API", () => {
    describe("GET BICICLETAS /", () => {
        it("Status 200", () => {

            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, "negro", "urbana", [4.099166, -76.178474]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
                console.log(response.statusCode);

            });

        });
    });

    describe("POST BICICLETAS /create", () => {
        it("Status 200", (done) => {
                var headers = {'Content-type' : 'application/json'};
                var aBici = '{"id": 10, "color":"rojo", "modelo":"urbana", "lat": 4, "lng": -76}';
                request.post({
                    headers : headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: aBici
                }, function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        expect(Bicicleta.findById(10).color).toBe("rojo");
                        done();
                });
        });

    });

    
    describe("POST BICICLETAS /delete", () => {
        //se crea la bicicleta
        it("Status 200", (done) => {
            var headers = {'Content-type' : 'application/json'};
            var aBici = '{"id": 10, "color":"rojo", "modelo":"urbana", "lat": 4, "lng": -76}';
            request.post({
                headers : headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("rojo");
                    done();
            });
        });
        //se elimina
        it("Status 204", () => {
                var aBici = '{"id": 10}';
                request.post({
                    url: 'http://localhost:5000/api/bicicletas/delete',
                    body: aBici
                });
        });

    });


   /* describe("PUT BICICLETAS /update", () => {
        //se crea la bicicleta
        it("Status 200", (done) => {
                var headers = {'Content-type' : 'application/json'};
                var aBici = '{"id": 11, "color":"azul", "modelo":"montaña", "lat": 4.54, "lng": -76.2}';
                request.post({
                    headers : headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: aBici
                }, function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        expect(Bicicleta.findById(10).color).toBe("rojo");
                        done();
                });
        });

        //se actualiza la bicicleta
        it("Status 200", (done) => {
                var headers = {'Content-type' : 'application/json'};
                var aBici = '{"id": 11, "color":"dorado", "modelo":"urbano", "lat": 4.54, "lng": -76.2}';
                request.post({
                    headers : headers,
                    url: 'http://localhost:5000/api/bicicletas/update',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("dorado");
                    done();
                });

        });


    });*/

});