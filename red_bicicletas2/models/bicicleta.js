var Bicicleta = function (id, color, modelo, ubicacion){
    this.id =id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function (){
    return 'id:' + this.id + "| color: " + this.color; 
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
    console.log('Entra arreglo en models bicicleta.js');
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId) {
    console.log('la variable aBiciId es ='+aBiciId);
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici
    else
        throw new Error ('No existe una bicicleta con el id ${aBiciId}');
    
}

Bicicleta.removeById = function (aBiciId) {
    console.log('Entra en modelo bicicleta removeById');
    for (var i=0; i<Bicicleta.allBicis.length; i++){
        console.log('Entra en modelo bicicleta removeById' + i);
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
    
}



var a = new Bicicleta(1, "rojo", "urbana", [4.083983, -76.186578]);
var b = new Bicicleta(2, "blanca", "urbana", [4.099166, -76.178474]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;

